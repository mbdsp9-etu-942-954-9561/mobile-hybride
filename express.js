const express = require('express');
const path = require('path');
const app = express();
const { createProxyMiddleware } = require('http-proxy-middleware');
// Serve static files from the 'dist' directory
app.use(express.static(path.join(__dirname, 'www')));

app.use('/api', createProxyMiddleware({ target: 'http://85.31.234.130', changeOrigin: true }));

// Define a catch-all route to serve 'index.html'
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'www/index.html'));
});

// Start the Express server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
