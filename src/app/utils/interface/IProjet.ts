import { IProjetCategory } from "./IAnnexe";

export interface IProjet {
  id?: number;
  title: string;
  _description: string;
  budget: number;
  startDate?: Date;
  projetCategory: IProjetCategory;
  dueDate?: Date;
}

export interface ProjetVote {
  projet: IProjet;
  canVote: boolean
  totalVote: number
  forVote: number
}
