export interface IAnnexe {
  id?: number;
  label: string;
}

export interface IProjetCategory {
  id?: number;
  label: string;
}
