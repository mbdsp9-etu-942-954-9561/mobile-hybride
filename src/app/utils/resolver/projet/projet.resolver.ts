import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {CitoyenService} from "../../service/citoyen/citoyen.service";
import {ICitoyen} from "../../interface/ICitoyen";
import {ProjetVote} from "../../interface/IProjet";

@Injectable({
  providedIn: 'root'
})
export class ProjetResolver implements Resolve<ProjetVote> {
  constructor(
    private service: CitoyenService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProjetVote> {
    const userJson = localStorage.getItem("user")!;
    const user = JSON.parse(userJson) as ICitoyen;
    let projetId = route.paramMap.get('id')!;
    return this.service.canVote(user.id, projetId);
  }
}
