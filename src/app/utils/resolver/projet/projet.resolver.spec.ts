import { TestBed } from '@angular/core/testing';

import { ProjetResolver } from './projet.resolver';

describe('ProjetResolver', () => {
  let resolver: ProjetResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(ProjetResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
