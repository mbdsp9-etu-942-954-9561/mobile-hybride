import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Settings} from "../../tools/Settings";
import {Observable} from "rxjs";
import {IProjet} from "../../interface/IProjet";

@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  baseUrl = "";

  constructor(private httpClient: HttpClient) {
    this.baseUrl = Settings.baseUrl + "projet/";
  }

  getList(): Observable<IProjet[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IProjet[]>(url);
  }

  create(projet: IProjet): Observable<IProjet> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IProjet>(url, projet);
  }
}
