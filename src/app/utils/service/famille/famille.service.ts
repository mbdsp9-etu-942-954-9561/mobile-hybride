import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Settings} from "../../tools/Settings";
import {Observable} from "rxjs";
import {IFamille} from "../../interface/IFamille";

@Injectable({
  providedIn: 'root'
})
export class FamilleService {
  baseUrl = "";

  constructor(private httpClient: HttpClient) {
    this.baseUrl = Settings.baseUrl + "famille/";
  }

  getList(): Observable<IFamille[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IFamille[]>(url);
  }

  create(famille: IFamille): Observable<IFamille> {
    const url = this.baseUrl + "create";
    console.log(url);
    console.log(JSON.stringify(famille))
    return this.httpClient.post<IFamille>(url, famille);
  }
}
