import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Settings} from "../../tools/Settings";
import {Observable} from "rxjs";
import {IProjet} from "../../interface/IProjet";
import {IInvestissement} from "../../interface/IInvestissement";

@Injectable({
  providedIn: 'root'
})
export class InvestissementService {

  baseUrl = "";

  constructor(private httpClient: HttpClient) {
    this.baseUrl = Settings.baseUrl + "investissement/";
  }

  getList(): Observable<IInvestissement[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IInvestissement[]>(url);
  }

  create(investissement: IInvestissement): Observable<IInvestissement> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IInvestissement>(url, investissement);
  }
}
