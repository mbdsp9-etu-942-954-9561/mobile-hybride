import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICitoyen, SignInDto} from "../../interface/ICitoyen";
import {Settings} from "../../tools/Settings";
import {ISondage} from '../../interface/ISondage';
import {ProjetVote} from "../../interface/IProjet";

@Injectable({
  providedIn: 'root'
})
export class CitoyenService {

  private baseUrl = "";
  private sondageUrl = "";

  constructor(private httpClient: HttpClient) {
    this.baseUrl = Settings.baseUrl + "citoyen/";
    this.sondageUrl = Settings.baseUrl + "sondage/";
  }

  login(admin: SignInDto): Observable<ICitoyen> {
    const url = this.baseUrl + "login";
    return this.httpClient
      .post<ICitoyen>(url, admin);
  }

  sondage(sondage: ISondage): Observable<ISondage> {
    const url = this.sondageUrl + "create";
    return this.httpClient
      .post<ISondage>(url, sondage);
  }

  canVote(idcitoyen: number, idProjet: number | string): Observable<ProjetVote> {
    const url = this.sondageUrl + "projetVote/" + idcitoyen + "/" + idProjet;
    return this.httpClient
      .get<ProjetVote>(url);
  }
}
