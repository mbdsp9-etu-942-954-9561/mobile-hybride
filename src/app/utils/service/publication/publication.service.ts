import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Settings} from "../../tools/Settings";
import {Observable} from "rxjs";
import {IPublication} from "../../interface/IPublication";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  baseUrl = "";

  constructor(private httpClient: HttpClient) {
    this.baseUrl = Settings.baseUrl + "publication/";
  }

  getList(): Observable<IPublication[]> {
    const url = this.baseUrl + "actualite";
    return this.httpClient.get<IPublication[]>(url);
  }

  create(publication: IPublication): Observable<IPublication> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IPublication>(url, publication);
  }
}
