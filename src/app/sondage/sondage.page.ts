import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {CitoyenService} from '../utils/service/citoyen/citoyen.service';
import {lastValueFrom} from 'rxjs';
import {ISondage} from '../utils/interface/ISondage';
import {ICitoyen} from "../utils/interface/ICitoyen";
import {ProjetVote} from "../utils/interface/IProjet";

@Component({
  selector: 'app-sondage',
  templateUrl: './sondage.page.html',
  styleUrls: ['./sondage.page.scss'],
})
export class SondagePage implements OnInit {

  constructor(private route: ActivatedRoute, private navCtrl: NavController, private citoyenService: CitoyenService, private activatedRoute: ActivatedRoute) {
  }

  user = {} as ICitoyen;


  ngOnInit() {
    const userJson = localStorage.getItem("user")!;
    this.user = JSON.parse(userJson) as ICitoyen;

    this.activatedRoute.data.subscribe(({projetVote}) => {
      this.projetVote = projetVote;
    })

  }

  selectedOption = 'oui';
  projetVote = {} as ProjetVote;

  submitSurvey() {
    let sondage: ISondage | null = null;

    if (this.selectedOption === 'oui') {
      sondage = {
        projet: this.projetVote.projet,
        citizen: this.user,
        choice: 1
      };
    } else if (this.selectedOption === 'non') {
      sondage = {
        projet: this.projetVote.projet,
        citizen: this.user,
        choice: 0
      };
    }
    this.citoyenService.sondage(sondage as ISondage).subscribe({
      next: async () => {
        this.projetVote = (await lastValueFrom(this.citoyenService.canVote(this.user.id, this.projetVote.projet.id!))) as ProjetVote;
      },
      error: (error) => {
        console.log(error.message);
      }
    })

  }
}
