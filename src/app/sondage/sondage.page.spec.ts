import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SondagePage } from './sondage.page';

describe('SondagePage', () => {
  let component: SondagePage;
  let fixture: ComponentFixture<SondagePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SondagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
