import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SondagePage } from './sondage.page';
import {ProjetResolver} from "../utils/resolver/projet/projet.resolver";

const routes: Routes = [
  {
    path: '',

    component: SondagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SondagePageRoutingModule {}
