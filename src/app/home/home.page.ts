import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {ProjetService} from '../utils/service/projet/projet.service';
import {IProjet} from '../utils/interface/IProjet';
import {lastValueFrom} from 'rxjs';
import {ICitoyen} from "../utils/interface/ICitoyen";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  iduser = -1;
  projets: IProjet[] = [];

  onItemClick(item: number | undefined) {

    setTimeout(() => {
      if (item) {
        this.navCtrl.navigateForward(`/sondage/${encodeURIComponent(item)}`)
      }
    }, 500);
  }

  constructor(private navCtrl: NavController, private projetService: ProjetService) {
  }

  ionViewWillEnter() {
    const userJson = localStorage.getItem("user")!;
    if (userJson !== null) {
      const user = JSON.parse(userJson) as ICitoyen;
      this.iduser = user.id;

    } else {
      this.navCtrl.navigateRoot('login');
      console.log('you are not LOGGED');
    }
  }

  async ngOnInit() {
    this.projets = await lastValueFrom(this.projetService.getList());
  }

  logout(){
    localStorage.removeItem("user");
    this.navCtrl.navigateRoot('login');
  }
}
