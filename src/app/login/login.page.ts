import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {CitoyenService} from "../utils/service/citoyen/citoyen.service";
import {ICitoyen, SignInDto} from '../utils/interface/ICitoyen';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  ngOnInit() {
  }
  username: string = '';
  password: string = '';

  constructor(private navCtrl: NavController,private citoyenService: CitoyenService) {}

  login() {

    const citoyen = {idcard: this.username, password: this.password} as SignInDto;

    this.citoyenService.login(citoyen).subscribe({
      next: (response) => {
        localStorage.setItem("user", JSON.stringify(response));
        this.navCtrl.navigateForward('/home');
        console.log('CITOYEN LOGGED') ;
      },
      error: (error) => {
        console.log(error.message);
      }
    })

  }
}
